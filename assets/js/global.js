import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import App from './components/index';
import reducers from './reducers/index';

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk))),
      rootElem = document.getElementById('app');

render(
  <Provider store={store}>
      <App/>
  </Provider>,
  rootElem
);