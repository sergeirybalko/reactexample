export const ADD_TODO = 'ADD_TODO',
             REMOVE_TODO = 'REMOVE_TODO',
             COMPLETE_TODO = 'COMPLETE_TODO',
             ASYNC_ADD_TODO = 'ASYNC_ADD_TODO',
             SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';

export const VISIBILITY_FILTERS = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETE: 'SHOW_COMPLETE',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
};

export const TEST_TODOS = [
    {
        id: '1',
        text: 'Lorem',
        complete: false
    },
    {
        id: '2',
        text: 'Rom',
        complete: false
    },
    {
        id: '3',
        text: 'Third',
        complete: false
    }
];