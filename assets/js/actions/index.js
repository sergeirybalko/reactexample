import { ADD_TODO, REMOVE_TODO, COMPLETE_TODO, SET_VISIBILITY_FILTER, ASYNC_ADD_TODO } from '../constant';

export const addTodo = (id, text) => {
    return {type: ADD_TODO, id, text}
};

export const asyncTodo = (arrayTodo) => {
  return {type: ASYNC_ADD_TODO, arrayTodo}
};

export const removeTodo = (id) => {
  return {type: REMOVE_TODO, id}
};

export const completeTodo = (id) => {
  return {type: COMPLETE_TODO, id}
};

export const setVisibilityFilter = (filter) => {
  return {type: SET_VISIBILITY_FILTER, filter}
};