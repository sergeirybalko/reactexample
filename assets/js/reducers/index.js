import { ADD_TODO, REMOVE_TODO, COMPLETE_TODO, SET_VISIBILITY_FILTER, VISIBILITY_FILTERS, ASYNC_ADD_TODO } from '../constant';
import { combineReducers } from 'redux';

const todos = (state = [], action) => {
    switch (action.type){
        case ADD_TODO:
            return[
                ...state, {
                    id: action.id,
                    text: action.text,
                    complete: false
                }

            ];
        case ASYNC_ADD_TODO:
            return[
                ...state,
                ...action.arrayTodo
            ];
        case REMOVE_TODO:
            return[
                ...state.splice(action.id, 1)
            ];
        case COMPLETE_TODO:
            return [
                ...state.map((item) => {
                    if(item.id === action.id){
                        return Object.assign({}, item, {
                            complete: !item.complete
                        })
                    }else{
                        return item;
                    }
                })
            ];
        default:
            return state;
    }
};

const visibilityFilter = (state = VISIBILITY_FILTERS.SHOW_ALL, action) => {
    switch (action.type){
        case SET_VISIBILITY_FILTER:
            return action.filter;
        default: return state;
    }
};

export default combineReducers({
    visibilityFilter,
    todos
});
