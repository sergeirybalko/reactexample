import React from 'react';
import Todo from './todo';

const TodoList = ({todos, onTodoClick}) => {
    let listTodos = todos && todos.map((currentTodo) => {
       return(
           <Todo key={currentTodo.id}
                 {...currentTodo}
                 onClick={() => onTodoClick(currentTodo.id)}/>
       )
    });
    return(
        <div className="wrap-todo-list">
            <ul className="todo-list">
                {listTodos}
            </ul>
        </div>
    )
};

export default TodoList;
