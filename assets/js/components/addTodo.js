import React from 'react';

const AddTodo = ({onAddClick}) => {
    let refs = {
      input: ''
    };
    const addTodo = (e) => {
      e.preventDefault();
      onAddClick(refs.input.value);
      refs.input.value = '';
    };
    return(
      <div className="wrap-add-todo">
          <form onSubmit={addTodo}>
              <input type="text" ref={node => refs.input = node}/>
              <button>{"Add todo"}</button>
          </form>
      </div>
    );
};

export default AddTodo;