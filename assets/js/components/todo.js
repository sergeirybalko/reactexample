import React from 'react';

const Todo = ({text, complete, onClick}) => {
    return(
        <li className={complete ? 'todo todo-completed' : 'todo'} onClick={onClick}>
            {text}
        </li>
    )
};

export default Todo;
