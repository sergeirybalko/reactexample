import React from 'react';
import { VISIBILITY_FILTERS } from '../constant/index';

const Footer = ({onFilterClick, addArrayTodos}) => {
    return(
        <div className="filterLinks">
            <button onClick={() => onFilterClick(VISIBILITY_FILTERS.SHOW_ALL)}>
                {"Show all"}
            </button>
            <button onClick={() => onFilterClick(VISIBILITY_FILTERS.SHOW_ACTIVE)}>
                {"Show active"}
            </button>
            <button onClick={() => onFilterClick(VISIBILITY_FILTERS.SHOW_COMPLETE)}>
                {"Show complete"}
            </button>
            <button onClick={() => addArrayTodos()}>
                {"Add 3 todos"}
            </button>
        </div>
    )
};

export default Footer;
