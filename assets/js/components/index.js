import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { addTodo, removeTodo, completeTodo, setVisibilityFilter, asyncTodo } from '../actions';
import { VISIBILITY_FILTERS, TEST_TODOS } from '../constant';
import AddTodo from '../components/addTodo';
import TodoList from '../components/todoList';
import Footer from '../components/footer';

class App extends Component {
    static propTypes = {
      visibleTodos: PropTypes.arrayOf(PropTypes.shape({
          text: PropTypes.string,
          complete: PropTypes.bool
      }))
    };
    addTodo(val){
        const { dispatch } = this.props;
        dispatch(addTodo(Date.now().toString(), val));
    }
    todoClickHandler(id){
        const { dispatch } = this.props;
        dispatch(completeTodo(id));
    }
    filterClickHandler(filter){
        const { dispatch } = this.props;
        dispatch(setVisibilityFilter(filter));
    }
    addArrayList(){
        const { dispatch } = this.props;
        setTimeout(()=>{
            dispatch(asyncTodo(TEST_TODOS));
        }, 2000);
    }
    render(){
        return(
            <div className="wrap-todo-app">
                <h1>{"Todo list"}</h1>
                <AddTodo onAddClick={::this.addTodo}/>
                <TodoList todos={selectTodos(this.props.visibleTodos, this.props.visibilityFilter)}
                          onTodoClick={::this.todoClickHandler}/>
                <Footer onFilterClick={::this.filterClickHandler}
                        addArrayTodos={::this.addArrayList}/>
            </div>
        )
    }
}

const selectTodos = (todos, filter) => {
    switch(filter){
        case VISIBILITY_FILTERS.SHOW_ALL:
            return todos;
        case VISIBILITY_FILTERS.SHOW_COMPLETE:
            return todos.filter(todo => todo.complete);
        case VISIBILITY_FILTERS.SHOW_ACTIVE:
            return todos.filter(todo => !todo.complete);
    }
};

const select = (state) => {
  return{
      visibleTodos: selectTodos(state.todos, state.visibilityFilter),
      visibilityFilter: state.visibilityFilter
  }
};

export default connect(select)(App);
